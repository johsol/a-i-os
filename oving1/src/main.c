#include "app.h"

// Entry point
int main()
{
    app_t* app = appCreate();
    appRun(app);
    appTerminate(app);

    return 0;
}
