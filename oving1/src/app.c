#include "app.h"
#include "input.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

//
//  Allocates and initializes a new App
//
//  returns the pointer tot he app.
//
app_t* appCreate()
{
    app_t* this = malloc(sizeof *this);
    
    this->menu = menuCreate();
    this->alarmcentral = alarmcentralCreate();
    this->running = 1;
    this->shownSongs = 0;
    
    return this;
}

void welcomeMessage() {
    time_t rawtime;
    struct tm * timeinfo;
    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    printf("Welcome to the alarm clock! It is currently %s", asctime (timeinfo));
}

//
//  Responsible for running an app instance.
//
void appRun(app_t* this)
{
    welcomeMessage();
    menuListOptions();
    
    while(this->running)
    {
        menuPrompt(this->menu, "> ");
        
        operation_t op = menuGetOperation(this->menu);
        if (op == MENU_NO_OPERATION)
        {
            printf("Not a valid command!\n");
            menuListOptions();
            continue;
        }
        
        alarmcentralRemoveFinishedAlarms(this->alarmcentral);
        
        if (op == MENU_SCHEDULE_NEW_ALARM) {
            appScheduleNewAlarm(this);
        } else if (op == MENU_LIST_ACTIVE_ALARMS) {
            appListActiveAlarms(this);
        } else if (op == MENU_CANCEL_SCHEDULED_ALARM) {
            appCancelScheduledAlarm(this);
        } else if (op == MENU_EXIT_APP) {
            appExitApp(this);
        }
    }
}

//
//  Helper function for scheduling a new alarm.
//
void appScheduleNewAlarm(app_t* this)
{
    time_t end_time = getTimeAndString("Schedule alarm at which date and time? ");
    time_t now = time(NULL);
    
    if (end_time < now) {
        printf("Please select a date and time in the future. Goes back to menu.\n");
        return;
    }
    
    if (!this->shownSongs) {
        printf("Sounds:\n");
        printf("\t1) Bells\n");
        printf("\t2) Jazz 1\n");
        printf("\t3) Jazz 2\n");
        printf("\t4) Bethoven\n");
        this->shownSongs = 1;
    }
    
    int sound = getInt("Choose sound: ") - 1;
    
    alarmcentralCreateNewClock(this->alarmcentral, end_time, sound);
}

//
//  Helper function for listing all active alarms.
//
void appListActiveAlarms(app_t* this)
{
    alarmcentralListAllAlarms(this->alarmcentral);
}

//
//  Helper function for cancelling active alarmes.
//
void appCancelScheduledAlarm(app_t* this)
{
    int index = getInt("Cancel which alarm? ") - 1;
    alarmcentralCancelWithIndex(this->alarmcentral, index);
}

//
//  Helper function for exiting application.
//
void appExitApp(app_t* this)
{
    this->running = 0;
    printf("Goodbye!\n");
}

//
//  Destructor for cleaning up the application.
//
void appTerminate(app_t* this)
{
    menuTerminate(this->menu);
    alarmcentralTerminate(this->alarmcentral);
    free(this);
}
