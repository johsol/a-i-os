#include "input.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//
//  Gets a char from the user.
//
char getChar(const char* prompt) {
    char c;
    printf("%s", prompt);
    do {
        fflush(stdin);
        c = getchar();
    } while (c == '\n' || c == EOF);
    return c;
}

//
//  Gets an int from 1 to 99 from the user.
//
int getInt(const char* prompt) {
    printf("%s", prompt);
    
    int myInt = 0;
    char str[24];
    while (myInt <= 0 || myInt >= 100) {
        fflush(stdin);
        scanf("%s", str);
        myInt = atoi(str);
    }
    return myInt;
}

//
//  Gets a string from the user.
//
const char* getString(const char* prompt, size_t size) {
    char* input = malloc(sizeof(char) * size);
    fflush(stdin);
    scanf("%s", input);
    return input;
}

//
//  Gets a timeinfo stuct from the user.
//
struct tm getTime(const char* prompt) {
    int year, month, day, hours, minutes, seconds;
    printf("%s", prompt);
    while(scanf("%d-%d-%d %d:%d:%d", &year, &month, &day, &hours, &minutes, &seconds) != 6) {
        fflush(stdin);
    }
    struct tm timeinfo = {0};
    timeinfo.tm_year = year;
    timeinfo.tm_mon = month;
    timeinfo.tm_mday = day;
    timeinfo.tm_hour = hours;
    timeinfo.tm_min = minutes;
    timeinfo.tm_sec = seconds;
    return timeinfo;
}


time_t getLocalTime()
{
    time_t t;
    time(&t);
    struct tm* timeinfo = localtime(&t);
    return mktime(timeinfo);
}

time_t getTimeAndString(const char* prompt)
{
    struct tm timeinfo = {0};
    printf("%s", prompt);
    char input[100];
    fflush(stdin);
    fgets(input, sizeof(input), stdin);
    strptime(input, "%Y-%m-%d %H:%M:%S", &timeinfo);
    return mktime(&timeinfo);
}
