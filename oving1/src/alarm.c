#include "alarm.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

const char* EXECUTABLE_PATH = "/usr/bin/afplay";
const char* sounds[4] = {"sounds/Bell.mp3",
                        "sounds/Jazz1.mp3",
                        "sounds/Jazz2.mp3",
                        "sounds/Bet.mp3"};

//
//  Allocates and initializes an alarm instance.
//
alarm_t* alarmCreate()
{
    alarm_t* this = malloc(sizeof *this);
    this->ringtone = "Ding ding";
    return this;
}

//
//  Letting the alarm ring.
//
void alarmRing(alarm_t* this)
{
    printf("%s\n", this->ringtone);
    if (this->sound>=0 && this->sound <=3) {
        execl(EXECUTABLE_PATH, EXECUTABLE_PATH, sounds[this->sound], (char*) NULL);
    }
}

//
//  Setting the ringtone of the alarm.
//
void alarmSetRingtone(alarm_t* this, const char* ringtone)
{
    this->ringtone = ringtone;
}

//
//  Setting the sound
//
void alarmSetSound(alarm_t* this, int sound)
{
    this->sound = sound;
}

//
//  Destructor that frees a alarm.
//
void alarmTerminate(alarm_t* this)
{
    free(this);
}
