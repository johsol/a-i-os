#ifndef alarmclock_h
#define alarmclock_h
#include "alarm.h"

#include <time.h>
#include <unistd.h>

typedef struct alarmclock {
    const char* name;
    alarm_t* alarm;
    pid_t pid;
    time_t sleep_time;
    time_t end_time;
    const char* timestampstring;
    int index;
} alarmclock_t;

alarmclock_t* alarmclockCreate(int, time_t);

void alarmclockBegin(alarmclock_t*);
void alarmclockCancel(alarmclock_t*);

const char* alarmclockTimestampString(alarmclock_t*);
void alarmclockPrint(alarmclock_t*);

void alarmclockSetSound(alarmclock_t*, int);

void alarmclockTerminate(alarmclock_t*);

#endif
