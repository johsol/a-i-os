#ifndef alarmcentral_h
#define alarmcentral_h

#include "alarmclock.h"

#include <time.h>
#include "input.h"

typedef struct alarmcentral {
    alarmclock_t** clockList;
    int clockSize;
} alarmcentral_t;

alarmcentral_t* alarmcentralCreate();

void alarmcentralTerminate(alarmcentral_t*);
void alarmcentralRemoveFinishedAlarms(alarmcentral_t*);

void alarmcentralCreateNewClock(alarmcentral_t*, time_t, int);
void alarmcentralCancelWithIndex(alarmcentral_t*, int);
void alarmcentralListAllAlarms(alarmcentral_t*);
int alarmcentralNextAvailableIndex(alarmcentral_t*);

#endif
