#include "alarmclock.h"
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include "input.h"

//
//  Allocates and initializes a new clock.
//
alarmclock_t *alarmclockCreate(int index, time_t sleep_time)
{
    alarmclock_t *this = malloc(sizeof(alarmclock_t));
    this->alarm = alarmCreate();
    this->index = index;
    this->sleep_time = sleep_time;
    return this;
}

//
//  Terminates and frees the alarm and the clock.
//
void alarmclockTerminate(alarmclock_t *this)
{
    alarmTerminate(this->alarm);
    free(this);
}

//
//  The alarm loop.
//
//  While the timer is not done and a stop is not issues, get the new time.
//  Only ring the alarm if the clock is not stopped.
//
void alarmclockFunc(alarmclock_t* this)
{
    time_t start = getLocalTime();
    time_t now;
    do{
        now = getLocalTime();
    } while (now - start < this->sleep_time);
    
    alarmclockPrint(this);
    printf(": ");
    alarmRing(this->alarm);
    
    exit(0);
}

//
//  Starts the clock.
//
//  The clock is started as its own process.
//
void alarmclockBegin(alarmclock_t *this)
{
    pid_t pid = fork();
    if (pid == 0) {
        alarmclockFunc(this);
    } else {
        this->pid = pid;
    }
}

void alarmclockCancel(alarmclock_t *this)
{
    kill(this->pid, 1);
}

void alarmclockPrint(alarmclock_t *this)
{
    printf("Alarm %d", this->index + 1);
}

void alarmclockSetSound(alarmclock_t* this, int sound)
{
    alarmSetSound(this->alarm, sound);
}

const char* alarmclockTimestampString(alarmclock_t *this)
{
    return this->timestampstring;
}
