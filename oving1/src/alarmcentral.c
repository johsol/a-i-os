#include "alarmcentral.h"
#include <stdlib.h>
#include <stdio.h>
#include "input.h"

//
//  Allocates and initialzies an Alarm Central
//
alarmcentral_t* alarmcentralCreate() {
    alarmcentral_t* this = malloc(sizeof *this);
    
    this->clockSize = 100;
    this->clockList = malloc(sizeof(alarmclock_t) * this->clockSize);
    
    for (size_t i = 0; i < this->clockSize; i++) {
        this->clockList[i] = (alarmclock_t*) NULL;
    }
    
    return this;
}

//
//  Destructor for cleaning up the Alarm Central
//
void alarmcentralTerminate(alarmcentral_t* this) {
    // Cleaning up clocks.
    for (int i = 0; i < this->clockSize; i++) {
        if (this->clockList[i] != NULL) {
            alarmclockTerminate(this->clockList[i]);
            this->clockList[i] = NULL;
        }
    }
    free(this);
}

//
//  Removes finished alarms from the list of alarms.
//
void alarmcentralRemoveFinishedAlarms(alarmcentral_t* this)
{
    for (int i = 0; i < this->clockSize; i++) {
        alarmclock_t* clock = this->clockList[i];
        
        if (clock != NULL) {
            int status = -1;
            waitpid(clock->pid, &status, 1);
            if (WIFEXITED(status)) {
                wait(&status);
                alarmclockTerminate(clock);
                this->clockList[i] = NULL;
            }
        }
    }
}

//
//  Gets the next available index in the clock list.
//
int alarmcentralNextAvailableIndex(alarmcentral_t* this)
{
    int index;
    for (int i = 0; i < this->clockSize; i++) {
        if(this->clockList[i] == NULL) {
            index = i;
            break;
        }
    }
    return index;
}

//
//  Creates a new clock from timeinfo.
//
void alarmcentralCreateNewClock(alarmcentral_t* this, time_t end_time, int sound)
{
    // Finds the delta-time.
    time_t current;
    time(&current);
    time_t dt = end_time - current;
    
    // Gets next position in clock list.
    int index = alarmcentralNextAvailableIndex(this);
    
    // Sets name of the Alarm
    char* name = malloc(sizeof(char) * 9);
    snprintf(name, index, "Alarm %d", index + 1);
    
    // Initialzies alarm and sets parameters.
    alarmclock_t* clock = alarmclockCreate(index, dt);
    clock->end_time = end_time;
    alarmclockSetSound(clock, sound);
    
    // Put the clock into the list
    this->clockList[index] = clock;
    
    
    // Start the clock
    alarmclockBegin(clock);
    printf("Scheduling alarm in %ld seconds.\n", dt);
}

//
//  Takes an index and tries to cancel the clock at that
//  position in the list.
//
void alarmcentralCancelWithIndex(alarmcentral_t* this, int index)
{
    if (this->clockList[index] == NULL) return;
    alarmclockCancel(this->clockList[index]);
    alarmclockTerminate(this->clockList[index]);
    this->clockList[index] = NULL;
}

//
//  Prints out all alarms in list.
//
void alarmcentralListAllAlarms(alarmcentral_t* this)
{
    for (int i = 0; i < this->clockSize; i++) {
        if (this->clockList[i] != NULL) {
            alarmclockPrint(this->clockList[i]);
            printf(" at %s\n", ctime(&this->clockList[i]->end_time));
        }
    }
}

