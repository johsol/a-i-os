#ifndef menu_h
#define menu_h

typedef struct menu
{
    char character;
} menu_t;

typedef enum operation {
    MENU_SCHEDULE_NEW_ALARM,
    MENU_LIST_ACTIVE_ALARMS,
    MENU_CANCEL_SCHEDULED_ALARM,
    MENU_EXIT_APP,
    MENU_NO_OPERATION
} operation_t;

menu_t* menuCreate();
void menuTerminate(menu_t*);

void menuPrompt(menu_t*, const char* prompt);
operation_t menuGetOperation(menu_t*);
void menuListOptions();


#endif
