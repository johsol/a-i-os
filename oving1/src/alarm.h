#ifndef alarm_h
#define alarm_h

typedef struct alarm {
    const char* ringtone;
    int sound;
} alarm_t;

alarm_t* alarmCreate();
void alarmRing(alarm_t*);
void alarmSetRingtone(alarm_t*, const char* ringtone);
void alarmTerminate(alarm_t*);
void alarmSetSound(alarm_t*, int);

#endif
