#ifndef application_h
#define application_h

#include "menu.h"
#include "alarmcentral.h"

typedef struct App
{
    menu_t* menu;
    alarmcentral_t* alarmcentral;
    int running;
    int shownSongs;
} app_t;


app_t* appCreate();
void appRun(app_t* this);
void appTerminate(app_t* this);

void appScheduleNewAlarm(app_t* this);
void appListActiveAlarms(app_t* this);
void appCancelScheduledAlarm(app_t* this);
void appExitApp(app_t* this);

#endif
