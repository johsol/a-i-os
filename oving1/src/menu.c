#include "menu.h"
#include <stdlib.h>
#include <stdio.h>
#include "input.h"

//
//  Allocates and initialzies a menu object.
//
menu_t* menuCreate()
{
    menu_t *menu = malloc(sizeof *menu);
    
    menu->character = " ";
    
    return menu;
}

//
//  Prompts the user and stores result in a menu struct.
//
void menuPrompt(menu_t *this, const char *prompt)
{
    char c = getChar(prompt);
    this->character = c;
}

//
//  Returns the operation that was previously input by user.
//
operation_t menuGetOperation(menu_t* this) {
    if (this->character == 's') {
        return MENU_SCHEDULE_NEW_ALARM;
    } else if (this->character == 'l') {
        return MENU_LIST_ACTIVE_ALARMS;
    } else if (this->character == 'c') {
        return MENU_CANCEL_SCHEDULED_ALARM;
    } else if (this->character == 'x') {
        return MENU_EXIT_APP;
    } else {
        return MENU_NO_OPERATION;
    }
}

//
//  Listing the options that are available to the user.
//
void menuListOptions()
{
    printf("Please enter \"s\" (schedule), \"l\" (list), \"c\" (cancel), \"x\" (exit)\n");
}

//
//  Destructor for the menu.
//
void menuTerminate(menu_t *this)
{
    free(this);
}
