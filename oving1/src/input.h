#ifndef input_h
#define input_h
#include <stdlib.h>
#include <time.h>

struct time_output {
    time_t t;
    char* timestamp;
};

char getChar(const char* prompt);
int getInt(const char* prompt);
const char* getString(const char* prompt, size_t size);
struct tm getTime(const char* prompt);
time_t getLocalTime();
time_t getTimeAndString(const char* prompt);

#endif
